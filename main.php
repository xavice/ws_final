<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="/css/main.css">
	
</head>
<body>
	<header>
		<div class="wrapper">
			<a href="/">WebSupport PHP zadanie</a>
			<div class="nav">
				<a href="/" class="nav__item">Home</a>
				<a href="/add-new" class="nav__item">Pridaj záznam</a>
			</div>
		</div>
	</header>
	<main>
		<div class="wrapper">
			<?php echo $sablona; ?>
		</div>
	</main>
	<footer>

	</footer>
	<script src="/js/app.js"></script>
</body>
</html>