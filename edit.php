<?php
if (isset($_POST['type'])) {

	$record = new Record($_POST);
	$result = $record->updateRecord();
	if ($result) {
		echo '<div class="alert alert--success">';
		echo 'Record updated.';
		echo "</div>";
	}
	$_record = $record->getParams();

} else {
	if (isset($URL_ADRESA[1])) {
		
		$record = new Record(['id' => $URL_ADRESA[1]]);
		$_record = $record->getRecord();
		if (!$_record) {
			// throw 404
			echo '<div class="alert alert--error">';
			echo 'Record not found.';
			echo "</div>";
		}
	}
}

?>

<?php /* classy added, SRV, MX sluzia len na zapinanie/vypinanie inputov pre rozne druhy zaznamov*/ ?>
<h2>Úprava záznamu</h2>
<form class="form-new-record" action="" method="POST">
	<input type="hidden" value="<?php echo $_record['id']; ?>" name="id">
	<select class="form-item record-type" name="type">
		<option value="<?php echo valueIfExist($_record['type']) ?: valueIfExist($_POST['type']); ?>" ><?php echo valueIfExist($_record['type']) ?: valueIfExist($_POST['type']); ?></option>
	</select>
	<label class="form-item label" for="name">Meno domeny</label>
	<input class="form-item input" type="text" name="name" value="<?php echo valueIfExist($_record['name'])?>" required>

	<label class="form-item label" for="content">Obsah</label>
	<input class="form-item input" type="text" name="content" value="<?php echo valueIfExist($_record['content']); ?>" required>

	<label class="form-item label" for="ttl">TTL</label>
	<input class="form-item input" type="text" name="ttl" value="<?php echo valueIfExist($_record['ttl']); ?>">

	<label class="form-item label added SRV MX" for="prio">Priorita</label>
	<input class="form-item input added SRV MX" type="text" name="prio" disabled value="<?php echo valueIfExist($_record['prio']); ?>">

	<label class="form-item label added SRV" for="port">Port</label>
	<input class="form-item input added SRV" type="text" name="port" disabled value="<?php echo valueIfExist($_record['port']); ?>">

	<label class="form-item label added SRV" for="weight">Váha</label>
	<input class="form-item input added SRV" type="text" name="weight" disabled value="<?php echo valueIfExist($_record['weight']); ?>">

	<button class="add-new fw" type="submit">Uložiť</button>
</form>