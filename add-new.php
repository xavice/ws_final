<?php
if (isset($_POST['name'])) {
	$record = new Record($_POST);
	$record->createRecord();
}
?>

<?php /* classy added, SRV, MX sluzia len na zapinanie/vypinanie inputov pre rozne druhy zaznamov*/ ?>
<h2>Pridaj záznam</h2>
<form class="form-new-record" action="" method="POST">
	<label class="form-item label" for="type">Typ záznamu</label>
	<select class="form-item record-type" name="type" required>
		<option value="A" <?php echo 'A' == valueIfExist($_POST['type']) ? 'selected' : ''; ?>>A</option>
		<option value="AAAA" <?php echo 'AAAA' == valueIfExist($_POST['type']) ? 'selected' : ''; ?>>AAAA</option>
		<option value="MX" <?php echo 'MX' == valueIfExist($_POST['type']) ? 'selected' : ''; ?>>MX</option>
		<option value="ANAME" <?php echo 'ANAME' == valueIfExist($_POST['type']) ? 'selected' : ''; ?>>ANAME</option>
		<option value="CNAME" <?php echo 'CNAME' == valueIfExist($_POST['type']) ? 'selected' : ''; ?>>CNAME</option>
		<option value="TXT" <?php echo 'TXT' == valueIfExist($_POST['type']) ? 'selected' : ''; ?>>TXT</option>
		<option value="SRV" <?php echo 'SRV' == valueIfExist($_POST['type']) ? 'selected' : ''; ?>>SRV</option>
	</select>

	<label class="form-item label" for="name">Meno domeny</label>
	<input class="form-item input" type="text" name="name" value="<?php echo valueIfExist($_POST['name'])?>" required>

	<label class="form-item label" for="content">Obsah</label>
	<input class="form-item input" type="text" name="content" value="<?php echo valueIfExist($_POST['content']); ?>" required>

	<label class="form-item label" for="ttl">TTL</label>
	<input class="form-item input" type="text" name="ttl" value="<?php echo valueIfExist($_POST['ttl']); ?>">

	<label class="form-item label added SRV MX" for="prio">Priorita</label>
	<input class="form-item input added SRV MX" type="text" name="prio" disabled value="<?php echo valueIfExist($_POST['prio']); ?>">

	<label class="form-item label added SRV" for="port">Port</label>
	<input class="form-item input added SRV" type="text" name="port" disabled value="<?php echo valueIfExist($_POST['port']); ?>">

	<label class="form-item label added SRV" for="weight">Váha</label>
	<input class="form-item input added SRV" type="text" name="weight" disabled value="<?php echo valueIfExist($_POST['weight']); ?>">

	<button class="add-new fw" type="submit">Uložiť</button>
</form>