<?php
$records = new Record();
$result  = $records->listRecords();

?>

<div class="table">
	<div class="table-header row">
		<?php /* <div class="cell cell-small">id</div> */?>
		<div class="cell cell-small">type</div>
		<div class="cell cell-small">name</div>
		<div class="cell cell-2">content</div>
		<div class="cell cell-small hide-xs">ttl</div>
		<div class="cell cell-small desktop">prio</div>
		<div class="cell cell-small desktop">weight</div>
		<div class="cell cell-small desktop">port</div>
		<div class="cell cell-small">edit</div>
		<div class="cell cell-small">delete</div>
	</div>
	<?php if (isset($result->items)) { ?>
	<?php foreach ($result->items as $value) {?>
	<div class="row">
		<?php /* <div class="cell cell-small"><?php echo $value->id; ?></div> */?>
		<div class="cell cell-small"><?php echo $value->type; ?></div>
		<div class="cell cell-small"><?php echo $value->name; ?></div>
		<div class="cell cell-2"><?php echo $value->content; ?></div>
		<div class="cell cell-small hide-xs"><?php echo $value->ttl; ?></div>
		<div class="cell cell-small desktop"><?php echo $value->prio; ?></div>
		<div class="cell cell-small desktop"><?php echo $value->weight; ?></div>
		<div class="cell cell-small desktop"><?php echo $value->port; ?></div>
		<div class="cell cell-small"><a href="/edit/<?php echo $value->id; ?>"><i class="material-icons icon--green">mode_edit</i></a></div>
		<div class="cell cell-small"><a href="/delete/<?php echo $value->id; ?>"><i class="material-icons icon--red">delete_forever</i></a></div>
	</div>
	<?php }?>
	<?php }?>
</div>
<a class="add-new" href="add-new"><i class="material-icons add-new__icon">add_circle_outline</i></a>
