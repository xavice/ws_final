<?php
session_start();

/*
- v dokumentacii, pri MX a SRV zaznamoch je @prio, ktora ma pravdepodobne zly popis
- get record priklad so self namiesto id, chyba na konci /:id
 */

require_once 'class/Record.php';
require_once 'library/helpers.php';

$REQUEST_URI  = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$QUERY_STRING = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
$URL_ADRESA   = explode('/', substr($REQUEST_URI, 1));

$sablona = '';

/*
- akoze routing
 */

if ($URL_ADRESA[0] == 'add-new') {
	ob_start();
	require_once 'add-new.php';
	$sablona = ob_get_contents();
	ob_end_clean();
}

if ($URL_ADRESA[0] == 'delete' && (int) $URL_ADRESA[1]) {
	ob_start();
	require_once 'delete.php';
	$sablona = ob_get_contents();
	ob_end_clean();
}

if ($URL_ADRESA[0] == 'edit' && (int) $URL_ADRESA[1]) {
	ob_start();
	require_once 'edit.php';
	$sablona = ob_get_contents();
	ob_end_clean();
}

if (empty($URL_ADRESA[0])) {
	ob_start();
	require_once 'title.php';
	$sablona = ob_get_contents();
	ob_end_clean();
}

require_once 'main.php';
