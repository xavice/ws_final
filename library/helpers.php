<?php

function valueIfExist(&$var, $default = null)
{
	return isset($var) ? $var : $default;
}