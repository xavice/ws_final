<?php

class Record
{

	protected $id;
	protected $type;
	protected $name;
	protected $content;
	protected $ttl;
	protected $prio;
	protected $weight;
	protected $port;

	protected $curl = null;

	public function __construct(array $record = [])
	{
		$this->id      = valueIfExist($record['id']);
		$this->type    = valueIfExist($record['type']);
		$this->name    = valueIfExist($record['name']);
		$this->content = valueIfExist($record['content']);
		$this->ttl     = valueIfExist($record['ttl']);
		$this->prio    = valueIfExist($record['prio']);
		$this->weight  = valueIfExist($record['weight']);
		$this->port    = valueIfExist($record['port']);
	}

	public function getParams()
	{
		$resluts;

		foreach ($this as $key => $value) {
			if (!empty($value) && $key != 'curl') {
				$results[$key] = $value;
			}
		}

		return $results;
	}

	public function listRecords()
	{
		$this->getCurl('GET');

		$result = curl_exec($this->curl);

		return json_decode($result);
	}

	public function createRecord()
	{
		$this->getCurl('POST', $this->getParams());

		$result = curl_exec($this->curl);
		$status = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

		if ($status == 201) {
			header("Location: /");
			die();
		}

		$this->showErrors(json_decode($result));
	}

	public function updateRecord()
	{
		$this->getCurl('PUT', $this->getParams());
		
		$result = curl_exec($this->curl);
		$status = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

		if (!($status == 200 && json_decode($result)->status == 'success')) {
			$this->showErrors(json_decode($result));
			return false;
		}
		
		return true;
		
	}

	public function getRecord()
	{
		$this->getCurl('GET');
		$result = curl_exec($this->curl);
		$status = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

		if ($status != 200) {
			// throw 404
			return false;
		} else {
			return json_decode($result, true);
		}
	}

	public function deleteRecord()
	{
		$this->getCurl('DELETE');
		$result = curl_exec($this->curl);
		$status = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

		if ($status == 200) {
			header("Location: /");
			die();
		}

		$this->showErrors(json_decode($result));
	}

	public function getCurl($method, $post_fields = null, $domain  = 'php-assignment-2.eu', $username = 'ws-php-assignment-2', $password = 'TOeM0UZ9AY')
	{
		$headers = [
			'Content-Type: application/json',
			'Accept: application/json',
		];

		// get all records url
		// or get one record if id not empty
		if ($method == 'GET') {
			$url = 'https://rest.websupport.sk/v1/user/self/zone/' . $domain . '/record' . '/' . $this->id;
		}

		// create record url
		if ($method == 'POST') {
			$url = 'https://rest.websupport.sk/v1/user/self/zone/' . $domain . '/record';
		}

		// update record url
		if ($method == 'PUT') {
			$url = 'https://rest.websupport.sk/v1/user/self/zone/' . $domain . '/record/' . $this->id;
		}

		// delete record url
		if ($method == 'DELETE') {
			$url = 'https://rest.websupport.sk/v1/user/self/zone/' . $domain . '/record/' . $this->id;
		}

		//
		if (!$this->curl) {
			$this->curl = curl_init();
			curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($this->curl, CURLOPT_USERPWD, $username . ":" . $password);
			curl_setopt($this->curl, CURLOPT_TIMEOUT, 30);
			curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $method);
			curl_setopt($this->curl, CURLOPT_URL, $url);
			curl_setopt($this->curl, CURLOPT_ENCODING, 'gzip');
			curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
            // if (!empty($post_fields)) {
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($post_fields));
            // }
		} else {
			curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $method);
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($post_fields));
			curl_setopt($this->curl, CURLOPT_URL, $url);
		}
	}

	public function showErrors(stdClass $response)
	{
		if (isset($response->errors) && !empty($response->errors)) {
			echo '<div class="alert alert--error">';
			foreach ($response->errors as $var => $error) {
				echo $var;
				echo "<br>";
				echo "<ul>";
				foreach ($error as $error_msg) {
					echo "<li>$error_msg</li>";
				}
				echo "</ul>";
				
			}
			echo "</div>";
		} elseif ($response->message) {
			echo '<div class="alert alert--error">';
			echo $response->message;
			echo "</div>";
		}
	}
}
