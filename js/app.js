(function() {
	var recordType = document.getElementsByClassName("record-type");
	if (recordType.length > 0) {
		var selectedType = recordType[0].options[recordType[0].selectedIndex].value;
		if (selectedType == "MX" || selectedType == "SRV") {
			addInputs(selectedType);
		}
		recordType[0].addEventListener("change", addInputs, false);
	}

	function addInputs(e) {
		var type;
		
		if (e.target) {
			type = e.target.value;
		} else {
			type = e;
		}

		if (type == 'SRV') {
			var srvInputs = document.getElementsByClassName("SRV");
			for (i = 0; i < srvInputs.length; i++) {
				srvInputs[i].disabled = false;
				srvInputs[i].classList.add('active');
			}
		} else if (type == 'MX') {
			var mxInputs = document.getElementsByClassName("MX");
			for (i = 0; i < mxInputs.length; i++) {
				mxInputs[i].disabled = false;
				mxInputs[i].classList.add('active');
			}
		} else {
			var addedInputs = document.getElementsByClassName("added");
			for (i = 0; i < addedInputs.length; i++) {
				addedInputs[i].disabled = true;
				addedInputs[i].classList.remove('active');
			}
		}
	}
})();